#!/usr/bin/env python3

import csv
import gitlab

# python-gitlab config must be in user's homedir
gl = gitlab.Gitlab.from_config('vm', ['.python-gitlab.cfg'])
users_amount = 25

for i in range(1, users_amount+1):
    i_string = str(i)

    # trigger pipelines
    search_string = 'workshop-user' + i_string + '/workshop'
    project = gl.projects.get(search_string)
    project.pipelines.create({'ref': 'master'})

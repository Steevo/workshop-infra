#!/usr/bin/env python3

import csv
import gitlab

# python-gitlab config must be in user's homedir
gl = gitlab.Gitlab.from_config('vm', ['.python-gitlab.cfg'])
users_amount = 25
import_url = "https://gitlab.com/gitlab-workshops/serverless-workshop"

for i in range(1, users_amount+1):
    i_string = str(i)

    # create user
    try:
        user_data = {
            'email': 'workshop-user' + i_string + '@example.com',
            'username': 'workshop-user' + i_string,
            'name': 'workshop-user' + i_string,
            'password': 'Serverless1!',
            'skip_confirmation': True
        }

        user = gl.users.create(user_data)
        print("Created workshop-user" + i_string)
    except gitlab.exceptions.GitlabCreateError as e:
        print("INFO: User with email already exists. Continuing..." + str(e))
        pass

    # create project for user
    try:
        # get user id and object
        user_obj = gl.users.list(search='workshop-user' + i_string)[0]
        user_id = user_obj.id

        # add project for user
        project_data = {
            'user_id': user_id,
            'name': 'workshop',
            'import_url': import_url,
            'container_registry_enabled': True,
            'visibility': 'public'
        }

        project = user_obj.projects.create(project_data)
        print("Created project for workshop-user" + i_string)
    except gitlab.exceptions.GitlabCreateError as e:
        print("INFO: Project name already exists for user. Continuing..."
              + str(e))
        pass

    # read tm config from json file and place in the project's CI variables
    search_string = 'workshop-user' + i_string + '/workshop'
    project = gl.projects.get(search_string)

    tm_config = open("configs/workshop-user" + i_string
                     + ".json", 'r').read()

    project.variables.create({'key': 'TMCONFIG', 'value': tm_config})
    project.variables.create({'key': 'CI_DEPLOY_USER',
                              'value': 'workshop-user' + i_string})
    project.variables.create({'key': 'SECRET_TOKEN',
                              'value': 'loopdeloopandpull'})

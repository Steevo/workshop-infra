# Workshop infra

This is a set of tools for interacting with GitLab instances used for public
workshops.

## Requirements

* A GitLab Ultimate instance.

* A Kubernetes cluster installed at the **instance** level with the following
  software installed:
    * Tiller: allows installation of other components.
    * Ingress: Needed for application deployments into Kubernetes.
    * Prometheus: Monitoring for this cluster.
    * Runner: A runner will need to be installed here in order for the **delete namespaces**
      job in the **delete-accounts** stage to complete.

* A fairly large Kubernetes cluster to run your users' pipelines. As we will
  expect a high amount of users all triggering pipelines around the same time,
  we will need GitLab Runner to be installed manually (via the Helm chart) onto
  this cluster. Cluster size and worker node specs will vary depending on your
  workshop's workload.

## "Installing"

1. Import this project into a project on your GitLab instance.

2. Set two variables in **Settings** -> **CI/CD** -> **Variables** in the copy
   of this project on your GitLab instance:
    * `API KEY` which corresponds to a [Personal Access Token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html). You will need the `read_user`, `read_registry`, `read_repository`, and `write_repository` scopes.
    * `HOSTNAME` which corresponds to the FQDN of your GitLab Instance.

## Jobs Breakdown

1. The first job, **test**, just runs `pycodestyle` (formerly known as `pep8`),
   a python linter which ensures readability and basic syntax.

2. The second stage, **delete-accounts**, consists of two jobs which delete
   user accounts on the desired GitLab instance, as well as application
   deployments in kube clusters. Each job is a **manual** action. It is fine to
   trigger both jobs simultaneously.

3. The third stage, **seed-accounts**, consists of a job to seed user accounts
   and a project per-user. This job is a **manual** action. Trigger this job
   after the **delete-accounts** stage completes.

4. The fourth stage, **trigger-pipelines**, triggers a pipeline in each user's
   project to do scale testing on the runner cluster and instance cluster. Trigger
   this job after the **seed-accounts** stage completes.

## Roadmap

* Automation to stand up an instance, consisting of:
    1. A terraform plan to provision a virtual machine.
    2. Ansible playbook to do basic sane server configuration and install GitLab
       EE. I prefer Ansible as it does not require minion software to be
       installed on hosts, which makes full automation easier.

* Docs for a full run-through so that others can utilize this. The idea is to have
  a button people can push without too much prep to stand up an instance for
  workshops. It will be aimed at mid-level technical folks, but should be easy
  enough for someone fairly non-technical to accomplish with the right amount
  of documentation.

* Tests for python code to catch runtime errors, and better python in general.
